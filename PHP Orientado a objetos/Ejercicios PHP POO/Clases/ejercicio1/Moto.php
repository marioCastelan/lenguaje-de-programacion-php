<?php

//crea aqui la clase Moto junto con dos propiedades public
class Moto
{
	//declaracion de propiedades
	public $tipo;
	public $marca;
}
//inicializamos el mensaje que lanzara el servidor con vacio
$mensajeServidorMoto = '';
//crea aqui la instancia o el objeto de la clase Moto
//se instancia la clase Moto
$Moto1 = new Moto;

if (!empty($_POST)) {

	// recibe aqui los valores mandados por post y arma el mensaje para front 
	$mensajeServidorMoto = 'el servidor dice que ya escogiste el tipo y la marca de la moto: tipo = ' . $_POST['tipo'] . ' marca = ' . $_POST['marca'];
	$Moto1->tipo = $_POST['tipo'];
	$Moto1->marca = $_POST['marca'];
}
