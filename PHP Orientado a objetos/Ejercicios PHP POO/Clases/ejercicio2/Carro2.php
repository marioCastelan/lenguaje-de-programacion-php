<?php
//creación de la clase carro
class Carro2
{
	//declaracion de propiedades
	public $color;
	public $modelo;
	private $circula;

	//declaracion del método verificación
	public function verificacion($year)
	{
		if ($year < 1990) {
			$this->circula = 'No';
		} elseif ($year >= 1990 and $year <= 2010) {
			$this->circula = 'Revisión';
		} else {
			$this->circula = 'Si';
		}
	}
	// Getter par poder leer el atributo privado
	public function get_circula()
	{
		return $this->circula;
	}
}

//creación de instancia a la clase Carro
$Carro1 = new Carro2();

if (!empty($_POST)) {
	$Carro1->color = $_POST['color'];
	$Carro1->modelo = $_POST['modelo'];
	// Guarda el año de fabricacion:
	// Primero separa el año y el mes para solo quedarnos con el año
	// Despues convierte el resultado a un numero
	// Por ultimo llama al metodo de verficacion para actualizar el atributo privado
	$Carro1->verificacion(intval(explode('-', $_POST['fabricacion'])[0]));
}
