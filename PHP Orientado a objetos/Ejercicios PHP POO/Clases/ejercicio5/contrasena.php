<?php
//declaracion de clase Contrasena
class Contrasena
{
    //declaracion de atributos
    private $contrasena;
    //declaracion de metodo constructor
    public function __construct()
    {
        // Genera una contraseña de 4 letras mayúsculas
        echo 'Objeto  Contrasena instanciado <br>';
        $this->contrasena = chr(rand(65, 90)) . chr(rand(65, 90)) . chr(rand(65, 90)) . chr(rand(65, 90));
    }

    //declaracion de metodo destructor
    public function __destruct()
    {

        echo 'Objeto destruido <br>';
        // Muestra la contraseña cuando el objeto es destruido
        echo 'Contraseña: ' . $this->contrasena;
    }
}



//creacion de objeto de la clase
$contrasena1 = new contrasena();
