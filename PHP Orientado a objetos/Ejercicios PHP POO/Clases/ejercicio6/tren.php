<?php
include_once('transporte.php');
//declaracion de la clase hijo o subclase avion
class tren extends transporte
{

    private $numero_vagones;

    //sobreescritura de constructor
    public function __construct($nom, $vel, $com, $vag)
    {
        parent::__construct($nom, $vel, $com);
        $this->numero_vagones = $vag;
    }

    // sobreescritura de metodo
    public function resumenTren()
    {
        $mensaje = parent::crear_ficha();
        $mensaje .= '<tr>
                <td>Numero de vagones:</td>
                <td>' . $this->numero_vagones . '</td>				
            </tr>';
        return $mensaje;
    }
}
