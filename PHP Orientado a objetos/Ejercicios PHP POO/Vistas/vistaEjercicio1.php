<?php
// __________________________________________
// Codigo con inclusion de archivos
include('..\Clases\ejercicio1\Moto.php');
include('..\Clases\ejercicio1\Carro.php');


// __________________________________________
// Codigo sin inclusion de archivos
// class Carro
// {
// 	//declaracion de propiedades
// 	public $color;
// }


// //crea aqui la clase Moto junto con dos propiedades public
// class Moto
// {
// 	//declaracion de propiedades
// 	public $tipo;
// 	public $marca;
// }

// //inicializamos el mensaje que lanzara el servidor con vacio
// $mensajeServidor = '';
// $mensajeServidorMoto = '';

// //crea aqui la instancia o el objeto de la clase Moto
// $Moto1 = new Moto;

// $Carro1 = new Carro;

// if (!empty($_POST)) {

// 	//almacenamos el valor mandado por POST en el atributo color
// 	$Carro1->color = $_POST['color'];
// 	//se construye el mensaje que sera lanzado por el servidor
// 	$mensajeServidor = 'el servidor dice que ya escogiste un color: ' . $_POST['color'];


// 	// recibe aqui los valores mandados por post 
// 	$mensajeServidorMoto = 'el servidor dice que ya escogiste el tipo y la marca de la moto: tipo = ' . $_POST['tipo'] . ' marca = ' . $_POST['marca'];
// 	$Moto1->tipo = $_POST['tipo'];
// 	$Moto1->marca = $_POST['marca'];
// }

?>

<!DOCTYPE html>
<html>

<head>

	<link rel="stylesheet" href="../css/bootstrap.min.css">
	<link rel="stylesheet" href="../css/bootstrap-grid.css">
	<script type="text/javascript" src="../js/bootstrap.min.js"></script>
	<script type="text/javascript" src="../js/jquery-3.4.1.min.js"></script>
	<title>
		Indice
	</title>
</head>

<body>

	<input type="text" class="form-control" value="<?php echo $mensajeServidor; ?>" readonly>

	<!-- aqui puedes insertar el mesaje del servidor para Moto-->
	<input type="text" class="form-control" value="<?php echo $mensajeServidorMoto; ?>" readonly>

	<div class="container" style="margin-top: 4em">

		<header>
			<h1>Carro y Moto</h1>
		</header><br>
		<form method="post">
			<div class="form-group row">

				<label class="col-sm-3" for="CajaTexto1">Color del carro:</label>
				<div class="col-sm-4">
					<input class="form-control" type="color" name="color" id="CajaTexto1">
				</div>
				<div class="col-sm-4">
				</div>

				<!-- inserta aqui los inputs para recibir los atributos del objeto-->
				<label class="col-sm-3" for="CajaTexto2">Tipo de la moto:</label>
				<div class="col-sm-4">
					<input class="form-control" type="text" name="tipo" id="CajaTexto2">
				</div>
				<div class="col-sm-4">
				</div>
				<label class="col-sm-3" for="CajaTexto3">Marca de la moto:</label>
				<div class="col-sm-4">
					<input class="form-control" type="text" name="marca" id="CajaTexto3">
				</div>
			</div>
			<button class="btn btn-primary" type="submit">enviar</button>
			<a class="btn btn-link offset-md-8 offset-lg-9 offset-6" href="../index.php">Regresar</a>
		</form>

	</div>


</body>

</html>