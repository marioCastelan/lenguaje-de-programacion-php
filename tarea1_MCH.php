<!-- Autor: Castelan Hernandez Mario 
Descripción: Muestra una pirámide y un rombo de asteriscos de Font Awesome, 
generados por codigo php -->

<html>
  <head>
    <script src="https://kit.fontawesome.com/f9a9f0c4ad.js" crossorigin="anonymous"></script>
  </head>
  <body>
      <center>
      <?php
      $maximoFilas = 30; //Maximo numero de filas
      $simbolos = '';
      $triangulo1 = ''; 
      $triangulo2 = '';

      //Genera los triangulos para crear la piramide y el rombo
      for ($i = 0; $i < $maximoFilas; $i++) {
          $triangulo2 = $simbolos . "<br>" . $triangulo2;
          $simbolos .= '<i class="fas fa-asterisk"></i>';
          $triangulo1 .= $simbolos . "<br>";
      } 
      // Muestra piramide   
      echo "<br>";
      echo $triangulo1;
      // Muestra rombo
      echo "<br>";
      echo $triangulo1;
      echo $triangulo2;
    ?>
    </center>
  </body>
</html>


