# Formularios
## Descripción
Con los conocimientos adquiridos durante el curso del Lenguaje de programación PHP se realizo un sitio Web que contiene las siguientes páginas:
* Una página  para ingresar llamada login.php.
* Una página donde se muestre la información del usuario que se autenticó y abajo toda la información guardada en sesión llamada info.php.
* Una página donde se capture y guarde en sesión la información de alumnos formulario.php.

**A continuación se muestran imágenes de las distintas páginas:**


**Página de inicio de sesión**
![alt text](images/login.png)
**Página de información de los usuarios**
![alt text](images/info.png)
**Página para registro de nuevos usuarios**
![alt text](images/formulario.png)
## Autor
* Castelan Hernandez Mario
### Contacto
mario36010h@outlook.com 
