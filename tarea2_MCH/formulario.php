<?php
session_start();
// Redirige a la pagina de sesion si no encuentra una sesion activa
if (empty($_SESSION['sesionActual'])) {
    header("Location: ./login.php");
    exit;
}
?>

<!doctype html>
<html lang="es">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link href="css/style.css" rel="stylesheet">
    <title>form</title>



</head>

<body>
    <div class="shadow nav-bar">
        <ul class="nav justify-content-center topnav">
            <li class="nav-item">
                <a class="nav-link" href="./info.php">Inicio</a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="./formulario.php">Registrar Alumnos</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="./terminaSesion.php">Cerrar Sesión</a>
            </li>
        </ul>
    </div>

    <div class="registro-usuario shadow">
        <h2>Nuevo Alumno</h2>
        <form action="./registro.php" method="POST">
            <div class="form-group row py-2">
                <label for="num_cta" class="col-sm-2 col-form-label">Número de Cuenta</label>
                <div class="col-sm-10">
                    <input type="number" class="form-control" name="num_cta" placeholder="Número de Cuenta">
                </div>
            </div>
            <div class="form-group row py-2">
                <label for="nombre" class="col-sm-2 col-form-label">Nombre</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="nombre" placeholder="Nombre">
                </div>
            </div>
            <div class="form-group row py-2">
                <label for="primer_apellido" class="col-sm-2 col-form-label">Primer Apellido</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="primer_apellido" placeholder="Primer Apellido">
                </div>
            </div>
            <div class="form-group row py-2">
                <label for="segundo_apellido" class="col-sm-2 col-form-label">Segundo Apellido</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="segundo_apellido" placeholder="Segundo Apellido">
                </div>
            </div>
            <div class="form-group row py-2">
                <label for="inputPassword" class="col-sm-2 col-form-label">Género</label>
                <div class="col-sm-10">
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="genero" id="Radios1" value="M" checked>
                        <label class="form-check-label" for="Radios1">
                            Hombre
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="exampleRadios" id="Radios2" value="F">
                        <label class="form-check-label" for="Radios2">
                            Mujer
                        </label>
                    </div>
                    <div class="form-check disabled">
                        <input class="form-check-input" type="radio" name="exampleRadios" id="Radios3" value="O">
                        <label class="form-check-label" for="Radios3">
                            Otro
                        </label>
                    </div>
                </div>
            </div>
            <div class="form-group row py-2">
                <label for="fecha_nac" class="col-sm-2 col-form-label">Fecha de Nacimiento</label>
                <div class="col-sm-10">
                    <input type="date" class="form-control" name="fecha_nac">
                </div>
            </div>
            <div class="form-group row py-2">
                <label for="contrasena" class="col-sm-2 col-form-label">Contraseña</label>
                <div class="col-sm-10">
                    <input type="password" class="form-control" name="contrasena" placeholder="Contraseña">
                </div>
            </div>
            <div class="form-group py-2 text-center">
                <button type="submit" class="btn btn-primary btn-lg">Registrar</button>
            </div>
        </form>
        <?php
        // Revisa se intento registrar un alumno que ya esta registrado
        if ($_SESSION['registro_valido'] == false) {
            echo '<div class="alert alert-info text-center" role="alert">Número de cuenta ya registrado</div>';
        }
        ?>
    </div>

    <!-- Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</body>

</html>