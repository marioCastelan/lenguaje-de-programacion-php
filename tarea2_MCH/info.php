<?php
session_start();
// Redirige a la pagina de sesion si no encuentra una sesion activa
if (empty($_SESSION['sesionActual'])) {
    header("Location: ./login.php");
    exit;
} else {
    $_SESSION['registro_valido'] = true;
}
?>

<!doctype html>
<html lang="es">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link href="css/style.css" rel="stylesheet">
    <title>Info</title>



</head>

<body>
    <div class="shadow nav-bar">
        <ul class="nav justify-content-center topnav">
            <li class="nav-item">
                <a class="nav-link active" href="./info.php">Inicio</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="./formulario.php">Registrar Alumnos</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="./terminaSesion.php">Cerrar Sesión</a>
            </li>
        </ul>
    </div>
    <div class="info-usuario">
        <div class="justify-content-center">
            <div class="card mb-3 shadow">
                <div class="card-header bg-transparent"><?php echo $_SESSION['Alumno'][$_SESSION['sesionActual']]['nombre'] . " " . $_SESSION['Alumno'][$_SESSION['sesionActual']]['primer_apellido']; ?></div>
                <div class="card-body">
                    <h5 class="card-title">Información</h5>
                    <p class="card-text">Número de cuenta: <?php echo $_SESSION['sesionActual']; ?></p>
                    <p class="card-text">Fecha de nacimiento: <?php echo $_SESSION['Alumno'][$_SESSION['sesionActual']]['fecha_nac']; ?></p>
                </div>
            </div>
        </div>
    </div>
    <div class="info-usuario">
        <table class="table table-hover tabla-usuario">
            <thead>
                <tr>
                    <th scope="col"># Numero de Cuenta</th>
                    <th scope="col">Nombre</th>
                    <th scope="col">Fecha de Nacimiento</th>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($_SESSION['Alumno'] as $key => $value) {
                    echo '<tr>';
                    echo '<th scope="row">' . $key . '</th>';
                    echo '<td>' . $value['nombre'] . ' ' . $value['primer_apellido'] . ' ' . $value['segundo_apellido'] . '</td>';
                    echo '<td>' . $value['fecha_nac'] . '</td>';
                    echo '</tr>';
                };
                ?>
            </tbody>
        </table>
    </div>
    <!-- Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</body>

</html>