<?php
session_start();
?>

<!doctype html>
<html lang="es">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link href="css/style.css" rel="stylesheet">
    <title>Login</title>
</head>

<body class="login-body">
    <div class="login shadow">
        <form action="./iniciaSesion.php" method="POST">
            <h2>Login</h2>
            <div class="form-group">
                <label for="num_cta">Número de Cuenta</label>
                <input type="number" class="form-control" name="num_cta" aria-describedby="emailHelp" placeholder="Ingrese su No. Cuenta">
            </div>
            <div class="form-group py-3">
                <label for="contrasena">Contraseña</label>
                <input type="password" class="form-control" name="contrasena" placeholder="Ingrese su contraseña">
            </div>
            <div class="text-center">
                <button type="submit" class="btn btn-primary btn-lg">Entrar</button>
            </div>
        </form>
        <?php
        // Revisa si se intento iniciar con un numero de cuenta o contraseña incorrecto
        if (array_key_exists('sesion_valida', $_SESSION)) {
            echo '<div class="alert alert-info text-center" role="alert">El número de cuenta o contraseña es incorrecto</div>';
        }
        ?>
    </div>

    <!-- Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</body>

</html>