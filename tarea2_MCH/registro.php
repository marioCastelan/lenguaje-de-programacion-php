<?php
// Realiza el registro del formulario en la sesion
session_start();
// Revisa si el alumno que se esta registrando no esta registrado
if (!array_key_exists($_POST['num_cta'], $_SESSION['Alumno'])) {
    $_SESSION['Alumno'][$_POST['num_cta']] = $_POST;
    $_SESSION['registro_valido'] = true;
    // Regresa a la pagina principal
    header("Location: ./info.php");
    exit;
} else {
    // El alumno ya estaba registrado
    $_SESSION['registro_valido'] = false;
    header("Location: ./formulario.php");
    exit;
}
