<!-- Autor: Castelan Hernandez Mario 
Descripción: Expresiones Regulares con PHP. -->


<?php
// ________________________________________________________________________________________________________________________
// 1. Realizar una expresión regular que detecte emails correctos.
echo '<h3> 1. Realizar una expresión regular que detecte emails correctos.</h3>';
$email_pattern = '/^[A-Za-z0-9-\._]+@([A-Za-z0-9-_]+\.)+[A-Za-z0-9-_]{2,4}$/';
$email = 'mario36010h@outlook.com';
if (preg_match($email_pattern, $email)) {
  echo $email . ' es un email correcto';
} else {
  echo $email . ' no es un email correcto';
}
echo '<br>';
echo '<br>';
echo '<br>';
// ________________________________________________________________________________________________________________________
// 2. Realizar una expresión regular que detecte Curps Correctos
echo '<h3> 2. Realizar una expresión regular que detecte Curps Correctos.</h3>';
$curp_pattern = '/^[A-Z]{4}[0-9]{6}[A-Z]{6}[0-9]{2}$/';
$curp = 'CAHM991227HDFSRR04';
if (preg_match($curp_pattern, $curp)) {
  echo $curp . ' es una Curp correcta';
} else {
  echo $curp . ' no es una Curp correcta';
}
echo '<br>';
echo '<br>';
echo '<br>';
// ________________________________________________________________________________________________________________________
// 3. Realizar una expresión regular que detecte palabras de longitud mayor a 50 formadas solo por letras.
echo '<h3> 3. Realizar una expresión regular que detecte palabras de longitud mayor a 50 formadas solo por letras.</h3>';
$palabras_pattern = '/^[A-Za-z]{50,}$/';
$palabra = 'dfdsfdfdfdfsdHJFKNDFJDBJFHEIUFKLDVNDVMARIOVJDFDJKLFJDDJFGKS';
if (preg_match($palabras_pattern, $palabra)) {
  echo $palabra . ' es una palabra de longitud correcta';
} else {
  echo $palabra . ' no es una palabra de longitud correcta';
}
echo '<br>';
echo '<br>';
echo '<br>';
// ________________________________________________________________________________________________________________________
// 4. Crea una función para escapar los símbolos especiales.
echo '<h3> 4. Crea una función para escapar los símbolos especiales.</h3>';
$cadena = 'hola$mundo$prueba';
$simbolo = '$';
function escapar_simbolo($cadena, $simbolo)
{
  return preg_quote($cadena, $simbolo);
}
echo 'cadena = ' . $cadena;
echo '<br>';
echo 'simbolo = ' . $simbolo;
echo '<br>';
echo 'cadena con símbolos escapados = ' . escapar_simbolo($cadena, $simbolo);
echo '<br>';
echo '<br>';
echo '<br>';
// ________________________________________________________________________________________________________________________
// 5. Crear una expresión regular para detectar números decimales.
echo '<h3> 5. Crear una expresión regular para detectar números decimales.</h3>';
$decimal_pattern = '/^[0-9]+\.[0-9]+$/';
$decimal = '3.1416';
if (preg_match($decimal_pattern, $decimal)) {
  echo $decimal .  '  es un número decimal';
} else {
  echo $decimal .  '  no es un número decimal';
}
echo '<br>';
echo '<br>';
echo '<br>';
?>